# Cómo hacer un huerto urbano

![](images/portada_Como-hacer-un-espacio-maker.jpg)

## Introducción
Un huerto urbano comunitario es un lugar donde la tierra se cuida y las personas se cultivan. La ciudad reverdece en estos singulares espacios de gestión ciudadana donde sus participantes ingenian formas de decisión colectiva mientras se hacen cargo del cuidado de la ciudad. Los huertos nos provocan para aprender nuevas formas de habitar lo urbano y en ellos se liberan unas capacidades pedagógicas de la ciudad que no conocíamos.

Esta guía realiza una muy breve introducción a los aspectos básicos para arrancar un huerto urbano comunitario y que consigamos disfrutar la ciudad a la vez que la transformamos.

Los huertos urbanos comunitarios son una de las prácticas de innovación ciudadana más establecida en Madrid y otras ciudades de todo el mundo. Espacios ciudadanos en los que de manera abierta un grupo de vecinos cuidan el espacio de todos, diseñando y construyendo jardines y huertas. Lugares para el cultivo vegetal y también el cultivo social, generan no sólo bancales con producción ecológica que atrae las discusiones sobre gestión eficiente de energía o riego, sino que consolidan espacios de reunión e intercambio comunitario. Muchas veces son la excusa para poner en marcha en sus entornos próximos actividades que tienen que ver con la cultura al aire libre. Cines de verano, teatros, debates y charlas, pequeños conciertos, talleres de construcción o prácticas pedagógicas.

![](images/00-AVECHUCO-vitoria-(1).jpg)

Hay muchos tipos de huertos: de ocio, escolares o incluso en balcones. Pero los que a nosotros nos interesan y de los que vamos a escribir en esta guía son los huertos urbanos comunitarios. Y esta triple condición es lo que les hace tan prolíficos e interesantes, y les convierte en agentes transformadores de la ciudad, en un movimiento imparable.

### Son huertos
Espacios para el cultivo vegetal, jardines comestibles cuidados por vecinos. Su condición hortelana vincula fuertemente estos espacios con el medio ambiente, con las políticas sostenibles y con reflexiones que giran en torno a lo que comemos y su procedencia. Los huertos urbanos comunitarios suelen gestionarse de manera ecológica, intentando no utilizar pesticidas ni fertilizantes, sino aprender de los tiempos, los ciclos y la sabiduría natural para obtener los mejores resultados y conseguir espacios frescos y verdes que nacerán en medio del asfalto de la ciudad.

Para tener un huerto hermoso y disfrutar de buenas cosechas es igualmente imprescindible mantener el espacio, aprenderemos en esta guía la importancia de los cuidados como práctica ecológica urbana que se aplica de forma radical en estos espacios.

### Son urbanos
Proyectos ligados directamente a la ciudad y sus barrios. Son proyectos de renovación y mejora de solares degradados, en los que pensar los jardines y plazas involucrando a los vecinos y vecinas de forma directa. Los huertos urbanos contribuyen a pensar la ciudad en clave participativa, reclaman el empoderamiento ciudadano para diseñar la ciudad, otorgando mayor capacidad de gestión, pero también mayor compromiso, a los propios vecinos.

Un huerto urbano revaloriza la forma de estar en la ciudad, incrementa las posibilidades de usarla y disfrutar de ella. Nos permite a los ciudadanos auto-diseñar y construir nuestros lugares, descubrimos en los huertos miles de mobiliarios urbanos imaginativos.

También son espacios en los que recuperar lo doméstico en lo público o lo común, espacios en los que se come, se riega, se charla o se echa la siesta, y se demuestra que la ciudad es una extensión natural de nuestras casas. Los huertos son urbanos porque hacen a la ciudad aprender otras formas de crecer e incluso de decrecer.

### Son comunitarios
Lugares gestionados desde la creación de comunidad. Porque solo a través de conformar una comunidad fuerte podemos hacernos cargo y disfrutar de los huertos. Lugares que no son tutelados sino autónomos, donde los vecinos y vecinas toman las decisiones, pero no solos, sino juntos. Los huertos contribuyen a la organización social, a la creación de lazos y amistades nuevas, incrementa las relaciones en una ciudad donde cada vez es más difícil conocer gente. Además, estas comunidades suelen ser ricas en pluralidad, totalmente intergeneracionales y abiertas, buscan la diversidad de procedencias y culturas. Los huertos urbanos son espacios en los que todo el mundo puede participar e implicarse en la medida de sus posibilidades, en los que no sobra nunca nadie.

En realidad, la mayor parte de las personas que habita los huertos urbanos comunitarios no lo hacen para disponer de hortalizas o comer más sano, ni siquiera para mejorar el entorno en el que viven, sino porque es divertido. Participan para conocer gente, pasarlo bien en comunidad y disfrutar de forma sana las ciudades en las que vivimos.

El huerto urbano es una escuela de ciudad, en la que se aprende la teoría de cómo plantar tomates, o a distinguir entre la planta de un pepino y un calabacín, además de muchas experiencias que van desde la normativa urbana, hasta la mediación, o del diseño de mobiliario a la gestión del agua y la energía. Cada participante en un huerto urbano se forma a través del aprendizaje compartido por todo el grupo, no hay maestros sino que todos aportamos y recibimos.

![](images/01-aprendizaje-ESTAESUNAPLAZA.jpg)

No hay un modelo de huerto urbanos único, no existen dogmas. Esta guía solo pretende ser un recopilatorio de experiencias que funcionan como una infraestructura para que, quien la lea, pueda hacer una revisión crítica de la misma y, a partir de ella, proponer nuevos modelos de huerto. Desde luego, no todo está en la guía, muchas cosas sólo se aprenderán con el roce y la experiencia. Os aconsejamos preguntar siempre a otros hortelanos, pero también tener iniciativa y lanzaros al vacío a experimentar en estos laboratorios vegetales que son los huertos.

Si estás pensando en arrancar un nuevo huerto urbano o en apuntarte al de tu barrio, estás a punto de participar en una experiencia única. Habitarás en un lugar confortable y divertido junto a gente interesante y diversa, vivirás en un espacio fresco y verde que te dará de comer y de reír.

Pero este lugar no te lo va a regalar nadie, lo tienes que pensar, construir y regar tu mismo, junto a las personas con las que lo vas a compartir. Montar un huerto urbano comunitario es participar en diseñar y cuidar un jardín cerca de casa, con amigos nuevos y viejos, en los que sacar todo el jugo posible a la ciudad en la que vives, pero sobre todo es habitar un espacio en el que disfrutar.
