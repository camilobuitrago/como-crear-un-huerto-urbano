# Créditos

### Autoría

Esta guía ha sido elaborada por CIUDAD-HUERTO, un itinerario formativo que recoge los aprendizajes y experiencias que se están poniendo en juego en los huertos urbanos comunitarios de la ciudad de Madrid. Está formado por muchos agentes diferentes que trabajan en ámbitos de la pedagogía, la ciudad y los huertos urbanos. Forman parte de su estructura algunos hortelanos de la Red de Huertos Urbanos Comunitarios de Madrid, miembros de la Plataforma Ciudad Escuela, técnicos del área de educación ambiental del Ayuntamiento de Madrid y los profesionales del Huerto de Retiro. También forma parte fundamental del proyecto Intermediae, centro de arte situado en Matadero Madrid. Por supuesto también forman parte de Ciudad Huerto todas las personas que han participado en algunos de sus talleres o jornadas de aprendizaje, compartiendo conocimientos que pueden ser útiles en los huertos urbanos comunitarios.

### Licencia de uso

Las guías didácticas de La Aventura de Aprender están publicadas bajo la siguiente licencia de uso Creative Commons: CC-BY-SA 3.0. Reconocimiento – CompartirIgual (by-sa): que permite compartir, copiar y redistribuir el material en cualquier medio o formato, así como adaptar, remezclar, transformar y crear a partir del material, siempre que se reconozca la autoría del mismo y se utilice la misma licencia de uso.
