# Cómo hacer un huerto urbano

![](images/1-portada_Guia-LADA_Como-hacer-un-huerto-urbano.jpg)


En la guía LADA sobre 'Cómo hacer un huerto urbano' realizamos una breve introducción a los aspectos básicos para arrancar un huerto urbano comunitario y que consigamos disfrutar la ciudad a la vez que la transformamos.

Esta guía ha sido elaborada por CIUDAD-HUERTO, un itinerario formativo que recoge los aprendizajes y experiencias que se están poniendo en juego en los huertos urbanos comunitarios de la ciudad de Madrid. Está formado por muchos agentes diferentes que trabajan en ámbitos de la pedagogía, la ciudad y los huertos urbanos. Forman parte de su estructura algunos hortelanos de la Red de Huertos Urbanos Comunitarios de Madrid, miembros de la Plataforma Ciudad Escuela, técnicos del área de educación ambiental del Ayuntamiento de Madrid y los profesionales del Huerto de Retiro. También forma parte fundamental del proyecto Intermediae, centro de arte situado en Matadero Madrid. Por supuesto también forman parte de Ciudad Huerto todas las personas que han participado en algunos de sus talleres o jornadas de aprendizaje, compartiendo conocimientos que pueden ser útiles en los huertos urbanos comunitarios.
