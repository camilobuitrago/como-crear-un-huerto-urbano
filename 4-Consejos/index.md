# Consejos

- Para un primer contacto: invitad a la gente interesada a alguna actividad divertida y motivadora: un taller de semillado, un día de cosecha, una cena de celebración…
- A la pregunta 'qué es lo que hacéis aquí': intentad ser amables y explicar lo que hacéis y cómo podría colaborar.
- Para que las y los vecinos puedan acercarse al huerto: colocad en un lugar visible algún cartel con los horarios y forma de contacto.
- Avisad e implicad al vecindario: para cualquier actividad organizada en el huerto, para crecer en participación y anticiparnos a posibles molestias.
- Después de haber semillado en los alvéolos: regadlos con un pulverizador de agua para evitar que un chorro de agua se lleve las semillas.
- La lechuga es el comodín del huerto: debido a su ciclo de crecimiento medio y a que se asocia bien con casi todas las hortalizas.
- Para hacer un buen compost: hay que trocear bien los restos orgánicos, hacer una mezcla homogénea de restos húmedos y secos y voltearlo de vez en cuando.
- Dejad espacio suficiente entre los bancales: (al menos 60 cm) para trabajar en los compostadores, habilita una zona amplia con mesas y sillas.

*Pasado el solsticio de verano, planta las crucíferas el buen hortelano*

*Florecidos los majuelos, ya pasaron los hielos*
