# Materiales
### Gente
- El principal material para un huerto comunitario son las personas.
- Es muy aconsejable no empezarlo solo, sino buscar amigos o afines que conformen un núcleo duro inicial.

### Suelo
- Debéis elegir el sitio en que váis a empezar el huerto.
- Lo ideal es que tenga buen soleamiento y esté cerca de infraestructuras (agua o luz).

### Agua
- Bastante importante conseguirla, y cuanto más cómodo sea acceder a ella mejor.
- Lo ideal es tener un riego por goteo instalado en los bancales, es la manera más eficiente.

### Herramientas
- De elaboración propia, prestadas o compradas, da igual. Os van a facilitar mucho las cosas.
- Las más imprescindibles son las azadas y azadillas, picos y palas, rastrillos, horcas, tijeras de poda, cubos, regaderas y una carretilla.

### Plantas
- El huerto necesita especies vegetales que cuidar y ver crecer, son el paisaje del huerto, lo que lo hará más atractivo.
- Para arrancar el huerto necesitamos conseguir estas plantas en forma de semillas o de plantones.

### Modelo de Gestión
- Todo huerto urbano comunitario tiene su propio modelo de gestión, para repartir tareas, para organizar el huerto o incluso para diseñarlo entre todos.
- A veces es más intuitivo y otras veces más pensado. Lo ideal es probar, aprender y evolucionar, no fijar modelos cerrados sino estar abiertos a mejorar en todo momento.
