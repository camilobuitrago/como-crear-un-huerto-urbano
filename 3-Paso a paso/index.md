# Paso a paso

Los huertos urbanos comunitarios no se desarrollan a través de un proceso lineal, existen huertos que han empezado justo al revés que otros. ¿Es mejor encontrar el espacio primero o formar el grupo de personas que lo va a cuidar? Seguramente no hay una única respuesta.Tampoco son un práctica homogénea, todos los huertos urbanos comunitarios son diferentes entre sí, adquiriendo caracteres distintos que permiten hacer aún más rico el ecosistema del que forman parte.

#### Tipos de huertos urbanos
Existen huertos urbanos comunitarios con alta producción de hortalizas y otros donde la hortalizas dan igual y se acaban convirtiendo en jardines para hacer actividades culturales. Hay huertos urbanos en los que participan doscientos vecinos y otros en los que participan solo seis, y no es mejor uno que otro, son simplemente diferentes.

#### Qué tipo de huerto urbano montar
Cada huerto urbano tiene su librillo y no queremos que esta guía se convierta en una suerte de instrucciones cerradas para seguir al pie de la letra. Solamente hemos querido organizar una serie de temas clave, extraídos de los aprendizajes que nos hemos encontrado en muchos de los huertos urbanos que hemos conocido, diferentes cuestiones que puedan facilitar las cosas a los que quieren acercarse a este mundo y participar en un huerto urbano.

Desde luego esta guía nace con la misma vocación con la que nacieron los huertos al hackear las ciudades en las que vivimos. Cada lector deberá utilizarla críticamente y ampliar, modificar o encontrar otras soluciones, inventar nuevos caminos y formas. Un huerto se monta con lo que se tiene a mano por poco que sea. Esa es su filosofía principal.

Esta afirmación llega hasta el punto de que en la Red de Huertos Comunitarios de Madrid, alguno de los huertos no tiene ni espacio, y se monta itinerante cuando los vecinos se juntan en el barrio. Como hemos dicho en la introducción, hay muchísimos modelos de huertos diferentes y ninguno es más acertado que otro. Si tienes ganas de montar un huerto urbano comunitario no busques excusas para no hacerlo, inventa tu propio modelo, esa es parte de su magia.

![](images/11-aprendiendoriego-VICALVARO.jpg)

## 1. Gente y tierra

*¿Hay vida en los bancales antes de las lechugas? ¿Cómo es el Big Bang que da origen a las relaciones personales en los huertos? ¿Cómo se evoluciona hacia un huerto comunitario? ¿Los grupos inteligentes surgen por esporas o hay que semillarlos? ¿Cómo se crea la atmósfera de convivencia?*

En el ámbito urbano, nos encontramos con distintas tipologías de huertos (escolares, de ocio, de autoconsumo, productivos...). Todos ellos tienen en común el cultivo de hortalizas en espacios urbanos. Sin embargo, no se da presente una gestión colectiva y abierta del espacio, de los recursos, de las decisiones y de los trabajos. Precisamente para diferenciar esta dimensión colectiva, los huertos urbanos que fueron surgiendo en Madrid en la primera década de 2000, quisieron remarcar su condición comunitaria, añadiendo, de forma inherente, el elemento ecológico a su forma de cultivar, lo que aportaba a este movimiento de aspectos transformadores de los carecían los anteriores.

Huertos urbanos comunitarios los hay de muchos tipos. Se puede decir que cada uno de ellos es un universo propio. Sin embargo, hay una serie de elementos comunes y que quedaron patentes desde un primer momento en la constitución de la Red de Huertos Urbanos Comunitarios de Madrid (RedH Mad!):

- Son abiertos a la participación de quien quiera, sin que exista una mediación económica ni afán de lucro en ninguna de las actividad es que allí se realizan.
- Son autogestionados, con una gobernanza horizontal en la que las decisiones son tomadas por todas las personas que se sienten parte del huerto. La Asamblea es el órgano de decisión por antonomasia.
- El trabajo, el espacio, las herramientas, las cosechas, la gestión... se desarrollan de forma comunitaria. Si el huerto es abierto y pertenece al barrio, sus procesos son también abiertos y todo pertenece al común de los participantes.
- Se cultivan siguiendo prácticas agroecológicas, por la salud del medio y de las personas que lo cultivan. - Se cultivan alimentos sanos, en una tierra sana.
- Para poner en marcha un huerto urbano comunitario tan solo hace falta contar con un espacio urbano y con un colectivo de personas que quieren echar para adelante con el proyecto agro-comunitario.

Como no vamos a inventar nada que ya esté inventado, a continuación esbozamos algunas opciones que han tenido otros huertos y otras personas para conseguir poner en marcha un huerto urbano comunitario.

### 1.1. Las personas, la gente que forma el huerto
En un huerto comunitario, tan importante es el cultivo de las hortalizas como el cultivo de las relaciones que allí se dan. Sin gente no existe la comunidad y el proyecto quedaría cojo. Pero construir un colectivo no siempre es fácil. Antes de nada, lo más recomendable es echar un vistazo a nuestro alrededor, buscar agrupaciones sociales existentes y consolidados que pueden ayudar a conformar un grupo en torno al proyecto hortícola.

![](images/20-HUERTORETIRO.jpg)

Algunos colectivos consolidados desde los que se han creado huertos urbanos son:

**Asociaciones de vecinos o colectivos ecologistas**

Muchos huertos madrileños proceden de asociaciones vecinales entre cuyos miembros ha surgido la idea de formar un huerto para mejorar el barrio.

Igualmente, entre los miembros de asociaciones ecologistas han surgido espacios hortícolas complementarios a su actividad cotidiana.

**Centros sociales autogestionados**

Los centros sociales son edificios surgidos como resultado de cesiones legales de edificios o más habitualmente mediante okupaciones ilegales.

La agricultura urbana ha sido una práctica habitual en ellos, por eso en las cercanías del centro social autogestionado o en su lejanía, es usual que construyan huertos como parte de su actividad de intervención en la ciudad.

**Centros educativos**

Algunos colegios han sido la sede de huertos promovidos por madres y padres, en otras ocasiones la misma institución educativa ha promovido el huerto y otras veces ha sido un proyecto conjunto.

**Curso de formación**
En algunas ocasiones los participantes en un curso de temática hortícola han formado un huerto. Es una forma de poner en contacto a personas con inquietudes y conocimientos parejos.

**Huertos ya en marcha**
Donde grupos de hortelanos migran para extender la experiencia en otro lugar.

![](images/3-08-disenodehuerto-MONTECARMELO.jpg)

### 1.2. El terreno donde instalar el huerto
La historia de RedH Mad! en Madrid es significativa de la relevancia que ha tenido la utilización y ocupación de espacios vacíos y baldíos urbanos en la construcción de huertos y en la emergencia de lo que se ha constituido como un creciente movimiento ciudadano. Una parte significativa de ellos provenían de ocupaciones de solares sin uso, vacíos urbanos donde no había nada y que, con los huertos urbanos, se han convertido en espacios de convivencia para los barrios. Entre las opciones para conseguir un terreno encontramos:

**Okupación**

Ha sido la forma más clásica y castiza de ocupar terrenos para los huertos urbanos en Madrid en años recientes. La manera de proceder es buscar un solar vacío o un baldío en el barrio, una zona que no tenga uso. Lo mejor es que el suelo sea de propiedad pública y, si es posible, que sea un terreno destinado a uso verde. Conviene que la acción de ocupación sea rápida y organizada: ocupa el terreno, monta el huerto lo antes posibles y hazte visible en el barrio.

**Huertos regularizados**

Ciudades de toda España han comenzado a crear programas de huertos municipales de gestión comunitaria.

**En terreno de instituciones**

Ya puede ser un colegio, un centro cultural, una parroquia, una asociación, una empresa, una comunidad de vecinos…

Muchas instituciones disponen de espacios en los cuales es posible desarrollar huertos urbanos, no hay más que crear sinergias y ponerse manos a la obra.

**En huertos educativos**

Algunos huertos públicos están destinados específicamente a servir como espacios formativos y puntos de encuentro de personas interesadas por la agroecología urbana.

![](images/4-16-asambleadelared-HUERTOLADIS.jpg)

## 2. Diseño del huerto
*Tenemos el terreno del huerto pero nada más. Nos enfrentamos a la nada absoluta. ¿Qué hacer? ¿Cómo empezamos a cavar? ¿Hacemos bancales o hacemos surcos? ¿Construimos los bancales tan largos como la vida? ¿Podemos plantar encima del suelo duro del solar?*

Un huerto urbano comunitario es un lugar ordenado, en el que se ha diseñado el espacio de forma determinada para realizar mejor y más fácil la actividad hortícola. Los trazados sencillos nos ayudarán a la hora de planificar riegos, organizar las plantaciones y en general para sistematizar tareas de mantenimiento. Sin embargo, la eficiencia no debe estar reñida con la estética. Al fin y al cabo, un huerto urbano es un jardín que debería combinar especies comestibles y ornamentales.

Cuando diseñemos nuestro huerto conviene que dibujemos en un plano la superficie que tenemos y planeemos detenidamente algunas cuestiones importantes: ¿dónde ponemos el huerto? ¿qué sistema de cultivo empleamos? ¿qué elementos debemos construir?

### 2.1. Espacio: dónde pongo el huerto

Un huerto debe ubicarse en un espacio donde existan varias condiciones idóneas:

- Luz: orientado hacia el sur.
- Disponibilidad de agua.
- Que esté refugiado de los vientos.
- Que tenga poca pendiente.
- Que el suelo sea fértil: de color oscuro y fácil de romper.
- Que nos podamos mover con facilidad.

En el entorno urbano no siempre vamos a tener fácil encontrarlas. Los problemas de ubicación pueden ser subsanados, pero la falta de luz o de agua son los factores que más condicionan el crecimiento adecuado de las plantas.

### 2.2. Sistema de cultivos
Existen varios sistemas de cultivos: bancal elevado, bancal en superficie, surcos,... A la hora de decidirnos por uno u otro conviene tener en cuenta el espacio que tenemos, el sistema de riego que vamos a utilizar, el tipo y número de personas que acuden al huerto y las actividades paralelas que se desarrollan allí.

Como los huertos urbanos comunitarios son espacios de una superficie limitada, en los que la zona de cultivo suele ser reducida, en los que puede existir un grupo numeroso de personas que lo cuidan, al que ocasionalmente pueden acudir grupos numerosos (de escolares por ejemplo) y en los que se utiliza generalizadamente el riego por goteo, el sistema de cultivo más empleado y más eficiente es el bancal elevado.

Hay que tener en cuenta el tamaño del bancal. De ancho, tendrá una distancia que nos facilite llegar cómodamente al centro del mismo desde ambos lados. 120 cm de anchura suele ser una distancia estándar muy utilizada y recomendable. No conviene que sea muy largo pues nos moveremos peor por el huerto y nos dará pereza pasar al otro lado. Con 4-5 metros de largo es suficiente.

### 2.3. Elementos del huerto

En el huerto no todo son los cultivos. Conviene, con el plano del huerto en la mano, ir haciendo una lista de aquéllos elementos que nos son necesarios para realizar nuestra actividad. Una vez que nos hayamos puesto de acuerdo en qué elementos queremos (y cómo queremos que sea nuestro huerto), los ubicaremos en el plano.

Conviene ser realista y tener claro qué huerto queremos, para qué lo queremos y cuántas personas van a ayudar a construirlo. No tiene por qué hacerse un huerto de una vez. Se puede ir poniendo algunos elementos que son más necesarios y, con el tiempo, ir creciendo.

Algunos elementos que deben considerarse en el huerto urbano comunitario son:

- Bancales: número y dimensiones.
- Sistema de riego: arquetas y conducciones.
- Compostadores.
- Caseta para herramientas.
- Zona de jardín y zona de plantas medicinales y aromáticas.
- Zona estancial: mesas, sillas, bancos, sombra.
- Zona de semilleros e invernadero.
- Estanque.
- Zona de juegos.
- Aparcabicis.

![](images/5-13-cuidadostaller-VICALVARO.jpg)

## 3. Recursos
*¿El agua para regar cae del cielo? ¿Puedo sacar semillas de hortalizas compradas en supermercados? ¿Cuánto cuesta una azada o dónde puedo conseguir una gratis? ¿Es necesario un tractor para cultivar un huerto urbano? ¿Si no hay electricidad, dónde cargan el móvil los hortelanos y las huertanas?*

Además del terreno y las personas, un huerto requiere de otra suerte de recursos importantes que pueden facilitar mucho el trabajo: herramientas, suministros e infraestructuras que contribuirán a hacer de estos espacios un oasis donde los ciudadanos pueden involucrarse en la gestión del espacio urbano. El agua, las semillas y plantas, las herramientas para el cultivo y la construcción del huerto, son también cuestiones fundamentales. Conocer qué necesitamos, cómo lo podemos conseguir fácilmente o cómo podemos vivir sin ello son aspectos fundamentales para desarrollar un huerto.

### 3.1. Agua

El agua nos permite llenar de vida y plantas el solar que hayamos escogido, pero al mismo tiempo es un recurso escaso que tenemos que optimizar. A veces porque no tenemos más remedio debido a las complicaciones de llevar el agua a nuestra parcela, otras por el compromiso ecológico que obliga a un consumo responsable. Un huerto urbano medio puede consumir anualmente un volumen de agua que supone un desembolso de entre 700 y 1.000 euros.

En términos generales el acceso al agua es precario, si no tienes en el huerto un grifo del que puedas sacarla, existen otros métodos posibles. Algunos huertos han pinchando las bocas de riego cercanas, basta con una llave de T y una manguera, a veces incluso con el beneplácito de los trabajadores de mantenimiento de parques y jardines.

En otras ocasiones, los vecinos cercanos proporcionan agua desde sus casas. La última opción es acarrear agua, colocando depósitos que la almacenen. Además del riego, se pueden desarrollar sistemas para el reaprovechamiento, a través de captación de agua de lluvia, captadores de aguas excedentes y sistemas de depuración natural.

### 3.2. Semillas y plantas
Los huertos no vienen con las plantas de serie, hay que conseguirlas de una u otra forma. La manera más sencilla es semillando, para lo que necesitamos semillas, Muy a menudo los huertos han servido para conocer y recuperar semillas locales. En Madrid, hay diferentes iniciativas e instituciones que facilitan semillas como el Banco de Intercambio de semillas en Intermediae o el banco de la propia red de huertos.

En cada ciudad o comunidad autónoma existen proyectos que recuperan semillas ecológicas y que, muchas veces, las distribuyen gratuitamente. También funciona el intercambio entre hortelanos. Otra opción es conseguir semillas de lugares a los que viajamos o la compra de semillas en tiendas de barrio y viveros.

Otra alternativa al uso de semillas es el plantón. Hay plantones en algunos viveros públicos en grandes ciudades, y a veces los ponen en juego para distribuirlos por los huertos. También es posible conseguirlo mediante intercambios o gracias a los excedentes de otros huertos, a través de donaciones de asociaciones o mediante la compra en viveros especializados y cooperativas. En última instancia, podríamos también autoproducirlo mediante la construcción de semilleros e invernaderos propios. En Madrid, es posible conseguir plantón en el Huerto de Retiro y en el vivero público de la Casa de Campo.

![](images/6-14-casetaybancales-HUERTOLADIS.jpg)

### 3.3. Herramientas
Las herramientas de huerto y jardinería son fundamentales, trabajar con los instrumentos adecuados permite una vida más digna y un huerto más cuidado. Un conjunto mínimo requeriría azadas, palas, rastrillos, azadillas de mano, carretilla y guantes.

Para conseguirlas es posible comprarlas en tiendas especializadas o acudir a préstamos temporales de asociaciones. Otra opción es la autoproducción. Algunas herramientas son muy caras y complejas, como motocultores o desbrozadoras.

El proyecto Open Source Ecology es un ejemplo paradigmático que diseña bici-azadas o incluso tractores cuyos planos están publicados de manera abierta. No es necesario que cada hortelano tenga sus propias herramientas, una opción habitual son las herramientas comunes compartidas.

Hay otras herramientas que no están destinadas al cultivo pero que pueden ser muy útiles a la hora de construir infraestructuras para el huerto, entre ellas atornilladores eléctricos, martillos, destornilladores, sierras, etc.

![](images/7-04-hotel-de-insectos--HUERTORETIRO.jpg)

## 4. Cuidados
*¿Les queda algo de ecológicas a las uvas que nos llegan en febrero de Sudamérica o de las zanahorias que lo hacen de Italia? ¿Qué entienden exactamente por ecológico dos actores tan distintos como el Mercado y los movimientos sociales? ¿Existen diferentes tipos de agriculturas respetuosas? ¿Es posible producir de forma intensiva en modo ecológico?*

Son muchas las tareas necesarias para cuidar los cultivos de huerto. Incluirlas todas nos desbordaría. Una máxima del huerto ecológico es tener un suelo sano y una buena planificación de cultivos para así conseguir un cultivo sano y productivo. Conviene por tanto actuar desde la prevención, buscando las mejores formas de cultivo, procurándonos buenos sustratos y abonos orgánicos, y seleccionando las plantas más idóneas y los momentos adecuados de siembra y plantación. Los riegos tendrán que ser los adecuados para el tipo de suelo, el tipo de cultivo y la época del año. Si a todo esto le añadimos unos cuantos trabajos preventivos, tendremos unas plantas que serán la envidia del vecindario.

![](images/8-03-invernadero-GRAMA.jpg)

### 4.1. Planificación de cultivos
El primer cuidado que podemos dar a nuestro huerto se realiza en el momento de diseñar los cultivos. Hay que saber qué plantar, en qué bancales del huerto y qué momento del año. Existen multitud de tablas que nos indican los momentos adecuados de siembra y plantación, y que nos serán de gran ayuda. Lo mejor es hacernos con una tabla adaptada a la región en la que estemos. Posteriormente, conviene plantar con criterio. Para que nuestro huerto sea más productivo y cuidar la salud de nuestra tierra y de las plantas hay que tener en cuenta dos elementos: las rotaciones y las asociaciones de cultivos.

**Rotaciones**

Es muy sencillo: después de cada cultivo debemos evitar repetir el mismo. ¿Por qué? Para prevenir plagas, aprovechar mejor los nutrientes y mantener la fertilidad del suelo. Así cada parcela tendrá hortalizas diferentes en distintas épocas del año. Es lo que se conoce como rotaciones de cultivo, que pueden ser anuales o estacionales. Existe una amplia bibliografía que te puede ayudar a planificar las rotaciones.

Como punto de partida lo más sencillo es agrupar las plantaciones por grupos de familias y necesidades de nutrientes. Una clasificación puede ser:

- Hortalizas de fruto
- Hortalizas de raíz
- Hortalizas de hoja
- Leguminosas

**Asociaciones de plantas**

Al igual de lo que nos ocurre a los humanos, algunas hortalizas conviven bien entre ellas y se ayudan mutuamente, pero otras no conviene juntarlas porque dificultarán su crecimiento. Incluso, habrá plantas que aunque crezcan unas al lado de otras, no ejerzan ninguna interacción entre ellas, ni positiva ni negativa.

Incluir una lista de asociaciones de plantas beneficiosas o perjudiciales sería muy extenso para los objetivos de esta publicación. Lo mejor es ir a la bibliografía especializada y, sobre todo, ensayar y observar las cosas que ocurren en nuestro huerto.

![](images/9-02-construccioncomun-ADELFAS.jpg)

### 4.2. Preparación del suelo
Para que nuestro huerto se mantenga sin problemas a lo largo del tiempo, hay que empezar por la base: tenemos que crear un buen suelo. Si no damos importancia a este aspecto, lo pagaremos en el futuro. Esta preparación se suele realizar a finales del invierno, normalmente, desde finales de enero o principios de febrero. El objetivo es tener un suelo preparado para las siembras directas que hagamos en los meses de febrero y marzo.

El orden de tareas para preparar el suelo sería el siguiente:

- Rozado: Es la eliminación de vegetación superficial con la azada y limpieza de raíces con el rastrillo.
- Laboreo: Se realiza usando la azada (y, si es posible, la laya de doble mango) para remover el suelo. El esfuerzo dependerá de la compactación y tiempo que pudiera llevar sin trabajarse u oxigenarse ese suelo; pero debemos conseguir un mínimo de 50 centímetros de profundidad.
- Abonado: Puede realizarse antes o después del laboreo y suele hacerse antes de la primavera. El objetivo principal no es fertilizar las plantas sino fertilizar el suelo. En los primeros años del huerto, nuestras plantas crecerán fuertes y sin muchos problemas, ya que la tierra tiene los nutrientes intactos. Pero estos se irán agotando, por lo que conviene ir abonando regularmente, añadiendo estiércol y compost.
- Acolchado: Es aconsejable echar una última capa de mulching o acolchado ya sea de paja o restos de compost más duros, que sirva para retener la humedad, mantenerlo mullido y evitar costras y erosión en nuestra parcela de cultivo. Hay que evitar dejar el suelo desnudo de forma prolongada.

### 4.3. Riegos

Lo sabemos desde que íbamos a preescolar: las plantas necesitan agua para vivir. Y al huerto tenemos que proporcionársela de forma adecuada, ni mucha ni poca cantidad, la justa para cada tipo de planta. Además existen suelos con características muy diferentes (si son más arenosos o más arcillosos) y por tanto el agua está disponible de forma distinta en cada uno. Otra variable es la estación del año: en los meses fríos seguramente no necesitemos regar, sin embargo en los calurosos puede que tengamos que hacerlo a diario.

Podemos tener bien preparado el suelo, que hayamos hecho un buen diseño y que tengamos un excelente plantel de hortalizas, pero si no regamos de forma adecuada se nos puede echar a perder todo. Es necesario diseñar bien el sistema de riego, que llegue a toda la superficie del bancal y que sea eficiente (relación entre necesidades hídricas y agua consumida). El sistema más usado por su eficiencia y comodidad en los huertos urbanos comunitarios es la instalación de parrillas de riego por goteo, con un sistema automatizado a través de un programador.

## 5. Autoconstrucción huertana

*¿Qué muebles necesita un huerto? ¿Qué diferencia hay entre un tornillo pasante y un tirafondo? ¿Qué herramientas son necesarias para construir un compostador? ¿De dónde podemos sacar materiales para construir un espacio de sombra? ¿Se pueden copiar los muebles que encontramos en otros huertos? ¿Es bueno bajar todos los muebles que nos sobran en casa? Y, sobre todo, ¿todos los muebles tienen que estar hechos de palets?*

El mobiliario huertano es fundamental para mejorar los huertos y representa una de las actividades que más tiempo nos lleva. A la vez, es una de las tareas que más seguidores tiene y que más engancha tanto a neófitos como a expertos manitas. A través de estos cacharros se define la imagen y el paisaje de los huertos, y pueden posibilitar que mucha otra gente se acerque y participe en el espacio, atraídos por la belleza de los diseños o por lo ingenioso de la construcción. Las actividades desarrolladas en torno a la construcción y el diseño colaborativo convocan a mucha gente y permiten dar grandes impulsos a los huertos.

Los diseños de los huertos urbanos comunitarios suelen ser open source: objetos de código abierto que permiten ser replicados en otros espacios. De esta manera, cualquier hortelano puede compartir fácilmente los diseños que se optimizan y mejoran más rápidamente. Por ello es fácil encontrar manuales de instrucciones en internet o vídeos de Youtube que nos dan consejos para construirlos.

### 5.1. Mobiliario huertano

Son muchos los tipos de muebles que podemos desarrollar en huertos urbanos, pero los que más encontramos responden a necesidades hortícolas y sociales. Darle vueltas a los diseños nos permite realizar construcciones más duraderas o que optimicen más los materiales. Un listado sencillo de mobiliario huertano podría contener los siguientes elementos:

**MOBILIARIO**

BANCALES Y JARDINERAS
Hay muchos tipos de bancales pero uno de los tipos más utilizados en los huertos urbanos comunitarios son los bancales elevados. Utilizaremos para ellos tablas anchas y largas haciendo el marco del bancal y se sujetarán unas a otras a través de estacas clavadas en el terreno o con piezas metálicas de unión.

TUTORES Y NUDOS
Los marineros son expertos muy necesarios en los huertos. Muchas de las cosas que se tienen que hacer se pueden resolver de forma muy eficiente tirando de nudos. Especialmente para preparar tutores para algunas plantas.

MESAS DE CULTIVO
Bancales elevados sobre patas. Se utilizan en suelos muy malos o para conseguir accesibilidad. Es conveniente dar un mínimo de 40 cm de fondo de tierra. Importante hacer una base sólida y fuerte y unas patas con buenas diagonales para que no se deforme la mesa con el peso de la tierra.

BANCOS, SILLAS Y MESAS
Los muebles más prolíficos. Los lugares donde sentarse a descansar, a comer los productos de la huerta, los espacios de socializar y hablar con otras personas. Hay muchísimos detalles y ejemplos de muebles hand made para hacer bancos, sillas y mesas, hay mil formas diseñar estructuras y de utilizar los materiales, pero es importante que al final los objetos sean minimamente estables.

COMPOSTADORES
Contenedores de residuo orgánico que generarán mejoras para nuestros suelos. Los compostadores son muebles muy interesantes y que pueden diseñarse de muchas formas, en función del tipo que compostaje que queramos hacer, están llenos de puertas, tapas y bisagras

### 5.2. Materiales

Son muchos los materiales que podemos aprovechar para diseñar y construir mobiliario de huertos. Dada nuestra economía y la filosofía ecológica de los proyectos, utilizaremos materiales que podemos encontrar desechados en diferentes partes de la ciudad: cajas de fruta, tubos y tuberías, muebles ya hechos, depósitos y cubos, cuerdas y cinchas, superadobe, cañas, etc.

El material estrella en la construcción de huertos es la madera. Un material democrático por lo fácil que es de conseguir y que no necesita, en los niveles básicos, herramientas complejas o difíciles de encontrar. Hay diferentes tipos de madera en función de donde la encontremos.

**Palets**

Son los ladrillos del mobiliario de huerto, la madera más fácil de conseguir y encontrar. Suelen estar fabricados con maderas macizas, pino de baja calidad, pero pino al fin y al cabo. Podemos encontrar palets, si vamos con los ojos abiertos, en cualquier sitio. Especialmente atentos en contenedores de escombros o en obras, donde preguntando muchas veces tienen material excedente.

Para trabajar los palets es necesario aprender a desmontarlos y, para ello, necesitaremos herramientas como palancas o martillos pesados. A veces, podemos diseñar muebles sin necesidad de desmontar los palets o desmontando solo ciertas partes, lo que nos ahorrará trabajo.

**Maderas de bancos del Ayuntamiento**

A partir de la Operación Herminio, un proyecto realizado entre diferentes técnicos del Ayuntamiento de Madrid, desde hace dos años, todas las maderas de los bancos que se sustituyen en Madrid se acumulan en un almacén de Coslada. Ahora el Ayuntamiento reparte estas tablas por proyectos de ámbito comunitario e interés público, como los huertos urbanos comunitarios.

Si vives en Madrid, ponte en contacto con el Departamento de Educación Ambiental del Área de Medioambiente del Ayuntamiento y pregunta sobre la disponibilidad. Si no vives en Madrid, quizá va siendo hora de pensar qué se hace con las tablas viejas de los bancos de tu ciudad.

La madera de estas tablas es excepcional, por sus dimensiones y capacidad de utilización pero también por el tipo de madera, que suele ser de altísima calidad y muchísima durabilidad.

**Maderas recicladas en obras y rehabilitaciones**

En las obras, además de palets podemos recuperar otro tipo de maderas de alto interés: maderas y tableros utilizados en encofrados que no se van a reutilizar; tablas y tablones que han servido de subestructura en rehabilitaciones y que se han cortado a medida; maderas que tenían uso estructural en un edificio que se va a rehabilitar cambiando la estructura… Suelen ser maderas de secciones muy grandes, ideales para bancales o bancos macizos.

**Maderas compradas**

Además la madera también se puede comprar en distintos aserraderos, es un material bastante barato. Hay muchos tipos de madera dependiendo de su categoría y procedencia. Por supuesto, recomendamos maderas de Km Cero, con tratamientos ecológicos.

## 6. Gobernanza y gestión
*¿Cómo se organiza un huerto urbano? ¿Todo se cultiva entre todos o cada uno tiene una parcela? ¿Quién elige lo que se cultiva? ¿Cómo se deciden los horarios? ¿Quiénes pueden tener llave? ¿Cuánto dura la asamblea de un huerto? ¿Hay que hacerla todas las semanas?*

Un huerto comunitario es un espacio que nos interpela constantemente a tomar decisiones colectivas; ¿estamos preparados para ello? ¿hemos sido educados en la asertividad, la comunicación afectiva y respetuosa, la tolerancia de posturas e ideas? Seguramente no, pero aún así es posible (y necesario) convertir el camino y andanza colectiva en una camino de aprendizaje personal.

Cualquiera que participe en una reunión o asamblea de personas, comprobará lo fácil que es ofendernos, personalizar, malinterpretarnos, molestarse y, en definitiva, no entendernos, mientras en paralelo, comprobará lo difícil que es ir consolidando un grupo humano a partir de personas que conversan y tratan de crear comunidad.

Por lo tanto, en un huerto urbano comunitario el verdadero reto es ir creando una comunidad que vaya dando cuerpo al proyecto: hay que ir conociéndose, compartiendo tareas, aprender a organizarse, aprender a afrontar conflictos... y celebrar. Todo ello se suele y debe hacer gradualmente, dando al tiempo y al proceso la importancia que merecen, no obsesionándose con el resultado, y aceptando, que muchas veces la práctica grupal deja al descubierto nuestras carencias personales, y finalmente no hay que ver como un fracaso el que no todos estemos preparados para formar parte de proyectos de este tipo.

Para ello, como decíamos, es útil ver los procesos de agrupamiento como procesos de crecimiento personal, donde consigamos ir despegándonos gradualmente del ego (identidad, autoimportancia, experiencia personal..) y trabajando por los valores comunitarios (sentido de pertenencia, ética de cuidados, valor e identidad del grupo..), acercándonos a lo que muchas tribus y pueblos africanos llaman ubuntu: yo soy porque tú eres. Ese es quizá uno de los mayores logros del desarrollo comunitario.

Como bien apunta José Luis Escorihuela, 'Ulises', lo más importante es consolidar el grupo humano, pudiendo servir de orientación las 7 C, que fácilmente se convierten en alguna más: conocerse, comunicarse, coger confianza, conflictuar (discutir, debatir, deliberar..), consensuar, coordinarse, crear comunidad… y CELEBRAR.

![](images/11-17-cenaenelhuerto-CORNISA.jpg)

## 7. Comunicación

¿Cómo se puede dar a conocer el huerto para que mucha más gente participe? ¿Qué ventajas tiene que el huerto esté en Red? ¿Cómo se prepara un hortelano para no morir entre grupos de WhatsApp, grupos de email o una página web?

Un huerto comunitario es un espacio que nos interpelaba constantemente, nos ubica, literalmente, en un lugar excepcional de la ciudad y nos obliga a tomar decisiones colectivas sobre el espacio público urbano que habitamos. Así que no nos queda otra que comunicarnos como seres vivos que somos. Los huertos urbanos requieren de un sistema de comunicación interna entre sus participantes, y externa con aquellos usuarios que vienen al huerto como el que acude a un jardín a leer o a pasear o que solicitan el espacio para proponer alguna actividad al barrio.

### 7.1. Comunicación interna

**Espacios de organización**

Por lo general, los huertos urbanos convocan a sus miembros una vez al mes en asamblea para tratar los asuntos de convivencia o de planificación que necesitan resolverse por consenso (suele ser la toma de decisión usual, más que la adoptada por mayoría de votos). En huertos donde se dan más usos que los agrícolas (eventos, actividades culturales, etc.) se crean comisiones específicas que se reúnen con otra periodicidad a la de la asamblea según la urgencia de los temas a tratar.

**Grupos de correo electrónico**

En ellos se vuelcan mensajes de todo tipo referentes al universo del huerto. Para no saturar los correos personales, suelen ser temas urgentes que necesitan una rápida respuesta por parte del grupo. Siempre es interesante adoptar unas normas de uso sencillas para que la información por este canal no se convierta en una coctelera de asuntos y en foco de posibles malinterpretaciones.

**Grupos de WhatsApp**

Sí, otro más al que añadir a tus numerosos grupos. La utilización de grupos de mensajería instantánea en el teléfono móvil como WhatsApp o Telegram se ha generalizado en muchos huertos pues constituyen un canal rápido para la gestión de la comunicación en el día a día.

![](images/12-06-arreglarlaciudad-HUERTOCORNISA.jpg)

### 7.2. Comunicación externa

Además de gestionar la comunicación entre sus participantes, los huertos han necesitado normalmente establecer cauces de comunicación externa con su entorno. En algunas ocasiones, los primeros pasos de andadura del huerto han generado inquietud o desconfianza entre los vecinos más próximos al solar. En otras, los proyectos de huertos han requerido del apoyo de personas del barrio para salir adelante. En general, la comunicación externa resulta necesaria para difundir las actividades que se realizan en los huertos y para comunicar la filosofía del espacio.

**Redes sociales**

Prácticamente no hay huerto sin un blog, una cuenta en Twitter o una página en Facebook. Son muy importantes para ir documentando la historia del espacio:

- comunicar las actividades que van a ocurrir en él con antelación;
- recoger la opinión de los vecinos sobre el proyecto;
- conseguir nuevos miembros que se sumen a la experiencia de construcción colectiva;
para ir creando el relato de vuestra experiencia comunitaria.
- Aunque a veces la labor de llevar las redes sociales del huerto es rotativa, lo normal es que siempre haya un miembro que se convierta en el gestor de redes de nuestro huerto.

Un viaje por los blogs y las páginas de Facebook de las experiencias existentes pueden ser inspiradoras para implementar alguna estrategia de comunicación.

Entre ellas pueden destacarse la actividad en estos canales de espacios como El Campo de Cebada, Esta es una plaza, Aliseda 18, El Huerto de la Cornisa o La Ventilla.

**Comisiones de eventos y prensa**

Hay huertos que crean un grupo de personas que se encarga de gestionar las propuestas externas que llegan al huerto/espacio para celebrar actividades o para atender a los medios de comunicación que proponen hacer un reportaje sobre el huerto.

**Señalética**

Así denominamos al arte de comunicar a través de carteles o paneles informativos en el huerto y resultan muy útiles para ubicar al visitante que se acerca por primera vez. Suelen informar sobre horarios de apertura, días de trabajo comunitario, formas de contactar, filosofía del proyecto y normas de uso.

La realización de carteles es una estupenda ocasión para abrir el proyecto del huerto al barrio. Organizar una jornada para pensar los mensajes que se quieren transmitir, así como los soportes donde irán, permite que se sume gente nueva a la filosofía del trabajo colaborativo. No todo el mundo acudirá al huerto a plantar, es más, habrá habitantes del espacio que nunca cojan una azada. Pero como iréis descubriendo, son muchas las tareas que se pueden llevar a cabo en un huerto.

**Eventos**

Comidas populares, talleres formativos, conciertos, proyecciones de documentales, son actividades muy efectivas para captar aliados en el barrio y futuros participantes. Además, muchos huertos son invitados para realizar charlas o participar en mesas redondas y jornadas de distinto tipo.

**Material audiovisual**

La grabación de vídeos es siempre una herramienta muy potente para dar visibilidad al proyecto en Internet, redes sociales, foros académicos, etc.

Algunos huertos han hecho de esta estrategia todo un arte y podemos citar el caso de Esta es una plaza por la querencia de sus miembros a la actuación.
